
export default class Target {
    HOLIDAYS_PER_YEAR = [
    '01-01',
    '04-18', 
    '05-01',
    '05-26',
    '05-27',
    '06-06',
    '07-21',
    '07-22',
    '08-15', 
    '10-31',
    '11-01',
    '11-02', 
    '11-11',
    '11-15',
    '11-27',
    '11-28',
    '11-29',
    '12-25', 
    '12-26',
    '12-30'
]

    constructor(name) {
            this.name = name;
            this.data = {}
            this.ok_checks = 0
            this.wrong_checks = 0
            this.empty_hour = 0   
            this.tml = {
                cp: {},
                dp:{},
                nullList: [],
                DpListDays: []                                   
            }             
                           
    }

    /** TODO: add hourly data by target object */
    addCode(start, line) {

        const day = start.substring(8, 10)

        const specialKey = "0" + day

        const hourlyData = {
            "timestamp": start,
            "code": line.code,            
            "target": line.target,
            "check": line.check,            
            "info": line.info
            
        }
      
        if (!(specialKey in this.data)) this.data[specialKey] = [hourlyData]
        else {
            this.__additionControl(specialKey, hourlyData)               
            this.data[specialKey].push(hourlyData)
        }
        
        //if(!this.HOLIDAYS_PER_YEAR.includes(start.substring(5,10))) 
        this.__check_availability(hourlyData)
        this.__check_timeleness(hourlyData)

        
        
    }

    /**TODO: add some fake data when there is a gap of one hour or more */ 
    __additionControl(day, hourlyData){
        
        const dailyData = this.data[day]
        const lastData = dailyData[dailyData.length - 1].timestamp

        const timestampDiff = new Date(hourlyData.timestamp) - new Date(lastData)

        const diffInHour = Math.abs(timestampDiff) / 36e5

        if(diffInHour > 2 ){
            for(let i = 1; i < diffInHour; i++ ){
                const hour = lastData.substring(11,13)
                const missingAt = lastData.replace(hour, `${parseInt(hour) + i}`)


                const emptyData = {
                    "timestamp": missingAt,            
                    "target": hourlyData.target,            
                    "info": 'No data received at this time'
                }

                dailyData.push(emptyData)
                this.empty_hour++
            }
        }        
    } 
    
    __check_availability(hourlydata){
        const day_hour = hourlydata.timestamp.split('-').slice(-1)[0].split(':')[0]
        switch(hourlydata.target){
            case 'comesep#glassfish':
                if (hourlydata.code <= 2)  this.ok_checks++
                else this.wrong_checks++
                break
            default:
                if(hourlydata.code <= 1) this.ok_checks++
                else this.wrong_checks++
                break
        }
    }

    __check_timeleness(hourlyData){
        const [year , month, day_hour] = hourlyData.timestamp.split(':')[0].split('-')
        const [day, hour] = day_hour.split('T')

        const isComesep = hourlyData.target.includes('comesep')
        const key = isComesep ? `${day}:${hour}` :  day
        const NN = isComesep ? 119  : new Date (year, month, 0).getDate() * 2
        
        const timeliness = hourlyData.code === 0 ? 'cp' : hourlyData.code === 1 ? 'null' : hourlyData.code == 2 ? 'dp' : ''

        switch(timeliness){
            case 'null':
                this.tml.nullList.push(key)         
                break;
            case 'dp':
                const dpValue = isComesep ? 0.5 : 1
                if(!(key in this.tml.dp)) this.tml.dp[key] = dpValue                           
                if(this.tml.DpListDays.indexOf(key) === -1) this.tml.DpListDays.push(key)                
                break;
            case 'cp':
                const cpValue = 1 / NN
                if(!(key in this.tml.cp)) this.tml.cp[key] = cpValue                       
                break;                     
        }

    }

    
    analyzeTml() {     
        const tml = this.tml
        tml.nullList.forEach(e => {
            delete tml.cp[e]
            delete tml.dp[e]
        })
            
        let critical, successive

        successive = tml.DpListDays.length > 0 ? tml.DpListDays.reduce((a, b) => parseInt(b) - parseInt(a)) === 1 : false
         
        critical = successive || tml.DpListDays.length >= 5 
            
        const totalCp = Object.values(tml.cp).reduce((a, b) => a + b, 0) 
        const totalDp = Object.values(tml.dp).reduce((a, b) => a + b, 0)       

        return {            
            cp: Number(totalCp.toFixed(1)),
            dp: Number(totalDp.toFixed(1)),
            critical: critical          
           
        }
        
    }
}
