
const AVAILABILITY = [
    {
        name: 'R.103',
        target: 'SPENVIS',
        products: [
            'general#amproxy1-ae',
            'spenvis#webserver#ssa',
            'spenvis#webserver#main',
        ]

    }, {
        name: 'R.127',
        target: 'DLR',
        products: [
            'general#amproxy1-ae',
            'dlr-iam-rad#webserver#ssa'
        ]

    }, {
        name: 'R.131',
        target: 'MSSL',
        products: [
            'general#amproxy1-ae',
            'mssl#webserver#ssa'
        ]

    }, {
        name: 'R.134',
        target: 'COMESEP',
        products: [
            'general#amproxy1-ae',
            'comesep#server',
            'comesep#mysql',
            'comesep#webserver#ssa',
            'comesep#webserver#main',
            'comesep#glassfish'
        ]
    }, {
        name: 'R.135',
        target: 'SEPEM',
        products: [
            'general#amproxy1-ae',
            'sepem#webserver#main',
            'sepem#webserver#ssa',
        ]
    }, {
        name: 'R.136',
        target: 'SPM',
        products: [
            'general#amproxy1-ae',
            'spm#webserver#ssa',
            'spm#webserver#main'
        ]
    }

]

const TIMELINESS = [
    {
        name: 'R.134',
        target: 'COMESEP',
        products: [
            'comesep#soap'
        ]

    },  {
        name: 'R.136',
        target: 'SPM',
        products: [
            'spm#gif#temp',
            'spm#gif#dens'
        ]

    }

]



const calculateAvailability = (data, default_hour) => {    
    let targets = [...data]
    let ret_val = ''
    let availabilities = {}
   
    availabilities = {}
    AVAILABILITY.forEach(avbl => {
        const wrong_check_tot = [...avbl.products.map(product => targets.find(t => t.name === product).wrong_checks)].reduce((a, b) => a + b)
        const ok_checks = default_hour - wrong_check_tot
        ret_val += `\n${avbl.target}, ${ok_checks}/${default_hour}, ${parseFloat((ok_checks/default_hour) *100).toFixed( 2 )}%                                          `
     
    })
    return ret_val
}

const calculateTimeliness = (data, period) => {   
    let ret_val = ''
    
    TIMELINESS.forEach(tmln => {
        let cp_r = 0
        let dp_r = 0
        let  critical_r = false

        tmln.products.forEach(c => {
            const target = data.find(p => p.name == c)
            const {cp, dp, critical} = target.analyzeTml()
            cp_r += cp
            dp_r += dp
            critical_r = critical
        })

        ret_val += `${tmln.name}, ${period}, ${critical_r}, ${cp_r}, ${dp_r}, ${cp_r - dp_r} \n` 

    })
    
    return ret_val
}

export {calculateAvailability, calculateTimeliness}
