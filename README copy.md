# monitoring_v2
```
It is about an Interface that allows users to view monthly reports in JSON format on a table. The table data value is hourly colored bar representation. So, the color depends on the code set for the time or a time gap.
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
